import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonModule } from './common';
import { NewsModule } from './news/news.module';


@Module({
    imports: [
        NewsModule,
        TypeOrmModule.forRoot(),
        CommonModule],
    providers: [],

})
export class AppModule { }
