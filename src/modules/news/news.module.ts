import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonModule } from '../common';
import { NewsController } from './controller/news.controller';
import { Newsrepository } from './dto/newsrepository';
import { News } from './entities/news.entity';
import { NewsService } from './service/news.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([News]),
        CommonModule,
        HttpModule

    ],
    controllers: [NewsController],
    providers: [NewsService, Newsrepository],
})
export class NewsModule {
}
