
import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as sinon from 'sinon';
import { News } from '../entities/news.entity';
import { NewsService } from './news.service';
import { HttpModule } from '@nestjs/axios';
import { LoggerService } from '../../common';
import { Newsrepository } from '../dto/newsrepository';

describe('NewsService', () => {

    let service: NewsService;
    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [HttpModule],

            providers: [
                NewsService,
                LoggerService,
                Newsrepository,
                {
                    provide: getRepositoryToken(News),
                    useValue: sinon.createStubInstance(Repository),
                }
            ],
        }).compile();
        service = moduleRef.get<NewsService>(NewsService);
    });


    describe('chargeNews', () => {
        it('Should return code 200 from get http', async () => {
            const result = 200;
            jest.spyOn(service, 'chargeNews').mockImplementation(async () => result);
            expect(await service.chargeNews()).toBe(result);
        });
    });

    jest.setTimeout(10000);

    describe('find', () => {
        it('Should return an array of news', async () => {
            const result: News[] = [];
            jest.spyOn(service, 'find').mockImplementation(async () => result);
            expect(await service.find()).toBe(result);
        });
    });




});


