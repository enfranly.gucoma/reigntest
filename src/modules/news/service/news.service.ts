import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { Newsrepository } from '../dto/newsrepository';
import { News } from '../entities/news.entity';
import { Cron } from '@nestjs/schedule';
import { firstValueFrom } from 'rxjs';
import { LoggerService } from '../../common';

@Injectable()
export class NewsService {

    constructor(private newsRepository: Newsrepository,
        private http: HttpService,
        private logger: LoggerService) { }

    @Cron('5 * * * * *')
    async chargeNews() {
        try {
            let newsList = await (await firstValueFrom(this.http.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')));
            newsList.data.hits.map(async (news: any) => {
                if (await this.newsRepository.find(news.objectID)) {
                    await this.newsRepository.update(news.objectID, news);
                } else {
                    news.tag = news._tags.map((tag: string) => { return { name: tag }; });
                    await this.newsRepository.save(news);
                    console.log("Guardo", news.tag);
                }
            });
            return newsList.status;
        } catch (error) {
            this.logger.error(error.message);
        }
    }

    public async find(filter?: any): Promise<News[]> {
        return await this.newsRepository.getAll(filter);
    }

    public async deleteNew(objectID: number): Promise<Response> {
        try {
            let response = await this.newsRepository.delete(objectID);

            return response.affected ? response.raw : "0 registers deleted";
        } catch (error) {
            return error.message;
        }

    }

}

