import { Test, TestingModule } from '@nestjs/testing';
import { NewsController } from './news.controller';
import { News } from '../entities/news.entity';
import { NewsService } from '../service/news.service';
import { NewsQueryDto } from '../dto/newsquerydto';

describe('NewsController', () => {
    let controller: NewsController;

    const mockNewsService = {
        findAll: jest.fn(),
    };

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [NewsController],
            providers: [NewsService],
        }).overrideProvider(NewsService).useValue(mockNewsService).compile();

        controller = module.get<NewsController>(NewsController);
    });


    it('Should return an array of news', async () => {
        const result: News[] = [];
        const dto = new NewsQueryDto();
        jest.spyOn(controller, 'findAll').mockImplementation(async () => result);

        expect(await controller.findAll(dto)).toBe(result);
    });

});
