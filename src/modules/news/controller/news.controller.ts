import {
    Controller,
    Delete,
    Get,
    HttpStatus,
    Param,
    Query,
    UseGuards,

} from '@nestjs/common';
import { NewsService } from '../service/news.service';
import { ApiBearerAuth, ApiQuery, ApiResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { RestrictedGuard } from '../../common/security';
import { NewsQueryDto } from '../dto/newsquerydto';
import { MonthsEnum } from '../../common';
import { NewsDto } from '../dto/NewsDto';

@Controller('news')
@ApiTags('news')
@ApiBearerAuth()
@UseGuards(RestrictedGuard)
export class NewsController {
    constructor(private readonly newsService: NewsService) { }



    @Get()
    @ApiResponse({ status: HttpStatus.OK, isArray: true, type: NewsDto })
    @ApiUnauthorizedResponse({ description: 'Unauthorized' })
    @ApiQuery({ name: 'author', required: false })
    @ApiQuery({ name: 'title', required: false })
    @ApiQuery({ name: 'month', required: false, enum: MonthsEnum })
    @ApiQuery({ name: 'tag', required: false })
    async findAll(@Query() query: NewsQueryDto) {
        //console.log(query.author);
        return await this.newsService.find(query);
    }


    @Delete(':id')
    @ApiResponse({ status: HttpStatus.OK, isArray: true })
    deleteNew(@Param('id') objectid: string) {
        console.log(objectid);
        //console.log(query.author);
        return this.newsService.deleteNew(parseInt(objectid));
    }


}
