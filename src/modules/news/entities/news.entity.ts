import { Transform } from 'class-transformer';
import { BaseEntity, Column, Entity, OneToMany } from 'typeorm';
import { Tag } from './Tag.entity';

@Entity({ name: 'news' })
export class News extends BaseEntity {
    public static readonly NAME_LENGTH = 50;


    @Column({ name: 'title', nullable: true })
    public title: string;

    @Column({ name: 'url', nullable: true })
    public url: string;

    @Column({ name: 'author', nullable: true })
    public author: string;

    @Column({ name: 'points', nullable: true })
    public points: string;

    @Column({ name: 'story_text', nullable: true })
    public story_text: string;

    @Column({ name: 'comment_text', nullable: true })
    public comment_text: string;

    @Column({ name: 'num_comments', nullable: true })
    public num_comments: string;

    @Column({ name: 'objectID', primary: true })
    public objectID: number;

    @Column({ name: 'story_id', primary: false, nullable: true })
    public story_id: number;

    @Column({ name: 'story_title', nullable: true })
    public story_title: string;

    @Column({ name: 'story_url', nullable: true })
    public story_url: string;

    @Column({ name: 'parent_id', nullable: true })
    public parent_id: string;

    @Transform(({ value }) => value.map(({ name }: any) => name))
    @OneToMany(() => Tag, (tag) => tag.news, { cascade: true })
    public tag: Tag[];

    @Column({
        name: 'created_at',
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP',
    })
    public created_at: Date;

    @Column({
        name: 'created_at_i',
        nullable: true,
    })
    public created_at_i: string;

    @Column({
        name: 'deleted_at',
        type: 'timestamp',
        nullable: true,
    })
    public deleted_at: Date;

    public buildData() {
        return {
            author: this.author,
            comment_text: this.comment_text,
            created_at: this.created_at,
            num_comments: this.num_comments,
            parent_id: this.parent_id,
            points: this.points,
            story_id: this.story_id,
            story_text: this.story_text,
            story_title: this.story_title,
            story_url: this.story_url,
            title: this.title,
            url: this.url,
        };
    }
}
