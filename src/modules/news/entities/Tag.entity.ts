import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { News } from './news.entity';

@Entity({ name: 'tags' })
export class Tag extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'name' })
    public name: string;

    @ManyToOne(() => News, (news) => news.tag)
    news: News;
}