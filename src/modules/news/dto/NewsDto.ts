import { ApiProperty } from "@nestjs/swagger";
import { Tag } from "../entities/Tag.entity";

export class NewsDto {

    @ApiProperty()
    public title: string;

    @ApiProperty()
    public url: string;

    @ApiProperty()
    public author: string;

    @ApiProperty()
    public points: string;

    @ApiProperty()
    public story_text: string;

    @ApiProperty()
    public comment_text: string;

    @ApiProperty()
    public num_comments: string;

    @ApiProperty()
    public objectID: number;

    @ApiProperty()
    public story_id: number;

    @ApiProperty()
    public story_title: string;

    @ApiProperty()
    public story_url: string;

    @ApiProperty()
    public parent_id: string;

    @ApiProperty()
    public tag: Tag[];

    @ApiProperty()
    public created_at: Date;

    @ApiProperty()
    public created_at_i: string;
    @ApiProperty()
    public deleted_at: Date;

}