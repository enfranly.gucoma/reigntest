import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { endOfDay, startOfDay } from "date-fns";
import { Between, InsertResult, Like, Repository, UpdateResult } from "typeorm";
import { News } from "../entities/news.entity";
const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];
@Injectable()
export class Newsrepository {
    constructor(
        @InjectRepository(News) private usersRepository: Repository<News>,
    ) { }

    public getAll(query: any): Promise<News[]> {
        const where = <any>{ deleted_at: null };
        const take = query.take || 5;
        const skip = query.skip || 0;
        const tag = query.tag || '';

        if (query.month) {
            let date = new Date().setMonth(monthNames.indexOf(query.month));
            where['created_at'] = Between(startOfDay(date), endOfDay(date));
        }
        if (query.author) where['author'] = Like(`%${query.author}%`);

        if (query.title) where['title'] = Like(`%${query.title}%`);
        console.log({ ...where }, tag);
        let querye = this.usersRepository.createQueryBuilder("news")
            .leftJoinAndSelect("news.tag", "tag")
            .where({ ...where })
            .take(take)
            .skip(skip);
        if (query.tag) querye.andWhere("tag.name = :tag", { tag: query.tag });
        return querye.getMany();
    }

    public save(news: News): Promise<News> {
        return this.usersRepository.save(news);
    }

    public find(objectID: number) {
        return this.usersRepository.findOne(objectID);
    }

    public delete(id: number): Promise<UpdateResult> {
        return this.usersRepository.update(id, { deleted_at: new Date() });
    }

    public update(id: number, news: News): Promise<InsertResult> {
        return this.usersRepository.createQueryBuilder()
            .insert()
            .into(News)
            .values(news)
            .orIgnore(true)
            .execute();
    }
}
