import { MonthsEnum } from "../../common";

export class NewsQueryDto {

    title: string;
    author: string;
    month: MonthsEnum
    tag: string;

}