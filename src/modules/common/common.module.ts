import { Module } from '@nestjs/common';

import { LogInterceptor } from './flow';
import { configProvider, LoggerService } from './provider';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
    providers: [
        configProvider,
        LoggerService,
        LogInterceptor
    ],
    imports: [ScheduleModule.forRoot()
    ],
    exports: [
        configProvider,
        LoggerService,
        LogInterceptor
    ]
})
export class CommonModule { }
